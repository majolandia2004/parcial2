/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package Modelo;

/**
 *
 * @author Lab05pc09
 */
public class Pueblo {

    private String nombre;
    private int cantAdultos;
    private int cantJovenes;
    private int cantNiños;

    public Pueblo() {
    }

    public Pueblo(String nombre, int cantAdultos, int cantJovenes, int canNiños) {
        this.nombre = nombre;
        this.cantAdultos = cantAdultos;
        this.cantJovenes = cantJovenes;
        this.cantNiños = cantNiños;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantAdultos() {
        return cantAdultos;
    }

    public void setCantAdultos(int cantAdultos) {
        this.cantAdultos = cantAdultos;
    }

    public int getCantJovenes() {
        return cantJovenes;
    }

    public void setCantJovenes(int cantJovenes) {
        this.cantJovenes = cantJovenes;
    }

    public int getCantNiños() {
        return cantNiños;
    }

    public void setCantNiños(int cantNiños) {
        this.cantNiños = cantNiños;
    }

    @Override
    public String toString() {
        return "nombrePueblo{" + "nombre=" + nombre + ", cantAdultos=" + cantAdultos + ", cantJovenes=" + cantJovenes + ", cantNi\u00f1os=" + cantNiños + '}';
    }
    

        public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Pueblo pueblo = (Pueblo) obj;
        return cantAdultos == pueblo.cantAdultos && Double.compare(pueblo.cantJovenes, cantAdultos) == 0
                &&cantJovenes  == pueblo .cantNiños;
    }
 
        
        public int compareTo(Pueblo otraPueblo) {
    
        if (this.cantAdultos < cantJovenes) {
            return -1;
        } else if (this.cantJovenes > cantNiños) {
            return 1;
        }
     
        if (this.cantAdultos < cantNiños) {
            return -1;
        } else if (this.cantNiños > cantJovenes) {
            return 1;
        }
     
        
        return 0;

    }
}



